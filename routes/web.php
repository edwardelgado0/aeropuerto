<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('admin/aeronave','AircraftController');

Route::resource('admin/factura','FacturasController');

Route::post('admin/factura/ajax-show', 'FacturasController@ajax_show');
/*
//----------------- users --------------------------
Route::get('admin/users',function(){
	return view('admin.users.index');
});

Route::get('admin/users/create',function(){
	return view('admin.users.create');
});

Route::get('admin/users/show',function(){
	return view('admin.users.show');
});

/*
Route::get('admin/aeronave',function(){
	return view('admin.aeronave.index');
});
*/

//------------------aeronave --------------------


/*
Route::get('admin/aeronave/create',function(){
	return view('admin.aeronave.create');
});

Route::get('admin/aeronave',function(){
	return view('admin.aeronave.index');
});


Route::get('admin/aeronave/show',function(){
	return view('admin.aeronave.show');
});

//------------------aeronave --------------------

Route::get('admin/factura/create',function(){
	return view('admin.aeronave.tasas');
});
*/