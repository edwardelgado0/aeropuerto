<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aircraft;
use App\Http\Controllers\TasasController;

class FacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TasasController $tasasController)
    {
        $tasa = $tasasController->aterrizaje(38);
        return print($tasa);
        $aircrafts = Aircraft::all();
        //return view('admin.factura.index',compact('facturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.factura.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajax_show(Request $request,TasasController $tasasController)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            //$multa = $request->input('multa');

            $aterrizaje = $tasasController->aterrizaje($id);
            $data_response = [];

            if ($aterrizaje == null) {
                $data_response = [
                    'status' => 'error',
                    'msg' => 'No se consiguieron datos con el campo aeronave'
                ];
            } elseif (count($aterrizaje) > 0) {
                $data_response = [
                    'status' => 'success',
                    'aterrizaje' => $aterrizaje ,
                    'data' => [12,15,48,19]
                ];
            }

            return response()->json($data_response);
        }
    }   
}
