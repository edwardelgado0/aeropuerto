<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Avion;
class AvionController extends Controller
{
	//tarifas portuarias 
    public function aterrizaje($peso=0,$factor=0,$ut=0){
    	$factor = 1.66;
		//$peso = 10;#kg
		$ut = 691.4138; #bs
		$v_nacional = true;
		$diurno = true;

		if ($v_nacional) {
			# code... vuelo nacional
			if ($diurno) {
				# code...  vuelo nacional diurno
				$minimo_factor = (0.85 * $peso) * $ut;
				$minimo = 13.26 * $ut;
				if ($minimo_factor > $minimo) {
					# code...
					return 'minimo factor '.$minimo_factor.' minimo '.$minimo;
				} 
				elseif ($minimo_factor == $minimo) {
					# code...
					return 'igual'.$minimo_factor;
				}
				else {
					# code...
					return 'minimo'.$minimo. 'minimo factor '.$minimo_factor;
				}
			}
			else{
				#vuelo nacional Nocturno 
				$minimo_factor = (0.70 * $peso) * $ut;
				$minimo = 16.80 * $ut;
				if ($minimo_factor > $minimo) {
					# code...
					return 'minimo factor '.$minimo_factor.' minimo '.$minimo;
				} 
				elseif ($minimo_factor == $minimo) {
					# code...
					return 'igual'.$minimo_factor;
				}
				else {
					# code...
					return 'minimo'.$minimo. 'minimo factor '.$minimo_factor;
				}
			}
		}
		elseif ($v_internacional) {
			# code... vuelo internacional
			if ($diurno) {
				# code...  vuelo nacional diurno
				$minimo_factor = 1.66 * $peso * $ut;
				$minimo = 17.73 * $ut;
				if ($minimo_factor > $minimo) {
					# code...
					return $minimo_factor;
				} 
				elseif ($minimo_factor == $minimo) {
					# code...
					return $minimo_factor;
				}
				else {
					# code...
					return $minimo;
				}
			}
			else{
				#vuelo nacional Nocturno 
				$minimo_factor = 1.97 * $peso * $ut;
				$minimo = 22.14 * $ut;
				if ($minimo_factor > $minimo) {
					# code...
					return $minimo_factor;
				} 
				elseif ($minimo_factor == $minimo) {
					# code...
					return $minimo_factor;
				}
				else {
					# code...
					return $minimo;
				}
			}
		}
    }
    public function estacionamiento(){
    	$factor = 1.66;
		//$peso = 10;#kg
		$ut = 691.4138; #bs
		$v_nacional = true;
		$diurno = true;
		$a_puesto_fijo=true;

		if ($v_nacional) {
			# code... vuelo nacional
				# estacionamiento vuelo internacional con peso y hora 
				$minimo_factor = 0.18 * $peso * $ut * $hora;
				$minimo = 5.65 * $ut;
				if ($minimo_factor > $minimo) {
					# code...
					return 'minimo factor '.$minimo_factor.' minimo '.$minimo;
				} 
				elseif ($a_puesto_fijo) {
					//Puesto Fijo de Aeronaves con matricula Nacional
					return 5.65 * $ut;
				}
				elseif ($minimo_factor == $minimo) {
					# code...
					return 'igual'.$minimo_factor;
				}
				else {
					# code...
					return 'minimo'.$minimo. 'minimo factor '.$minimo_factor;
				}
			}
		elseif ($v_internacional) {
			# code... vuelo internacional
			# estacionamiento vuelo internacional con peso y hora 
			$minimo_factor = 0.22 * $peso * $ut * $hora;
			$minimo = 9.46 * $ut;
			if ($minimo_factor > $minimo) {
				# code...
				return $minimo_factor;
			} 
			elseif ($minimo_factor == $minimo) {
				# code...
				return $minimo_factor;
			}
			else {
				# code...
				return $minimo;
			}
		}
		
		else{
			return 'opcion invalida';
		}
    }
    public function avion(){
    	$avion = Avion::find(1);
    	return $this->aterrizaje($avion->peso,$avion->matricula);
    }
}
