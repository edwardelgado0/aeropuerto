<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aircraft;

class AircraftController extends Controller
{
    public function index(){
    	$aircrafts = Aircraft::all();
    	return view('admin.aeronave.index',compact('aircrafts'));
    }
    public function create(){
    	return view('admin.aeronave.create');
    }
    public function store(Request $request){
    	$aircraft = new Aircraft;
    	//return $request->all();
    	$aircraft->fill($request->all());
    	$aircraft->save();
    	return redirect('admin/aeronave/create')->with('message', 'Post saved');
    }
    public function show($id){
    	$aircraft = Aircraft::find($id);
    	return view('admin/aeronave/show',compact('aircraft'));
    }
}
