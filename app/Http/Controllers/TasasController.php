<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aircraft;
class TasasController extends Controller
{
    public function aterrizaje($id){
    	$aircraft = Aircraft::find($id);
    	$factor = 1.66;
		$this->peso = $aircraft->peso;#kg
		$ut = 691.4138; #bs
		$v_nacional = true;
		$diurno = true;

	if ($v_nacional) {
		# code... vuelo nacional
		if ($diurno) {
			# code...  vuelo nacional diurno
			$minimo_factor = (0.85 * $this->peso) * $ut;
			$minimo = 13.26 * $ut;
			if ($minimo_factor > $minimo) {
				# code...
				//return 'minimo factor '.$minimo_factor.' minimo '.$minimo;
				return $minimo_factor;
			} 
			elseif ($minimo_factor == $minimo) {
				# code...
				return $minimo_factor;
			}
			else {
				# code...
				//return 'minimo'.$minimo. 'minimo factor '.$minimo_factor;
				return $minimo;
			}
		}
		else{
			#vuelo nacional Nocturno 
			$minimo_factor = (0.70 * $this->peso) * $ut;
			$minimo = 16.80 * $ut;
			if ($minimo_factor > $minimo) {
				# code...
				//return 'minimo factor '.$minimo_factor.' minimo '.$minimo;
				return $minimo_factor;
			} 
			elseif ($minimo_factor == $minimo) {
				# code...
				return $minimo_factor;
			}
			else {
				# code...
				//return 'minimo'.$minimo. 'minimo factor '.$minimo_factor;
				return $minimo;
			}
		}
	}
	elseif ($v_internacional) {
		# code... vuelo internacional
		if ($diurno) {
			# code...  vuelo nacional diurno
			$minimo_factor = 1.66 * $this->peso * $ut;
			$minimo = 17.73 * $ut;
			if ($minimo_factor > $minimo) {
				# code...
				return $minimo_factor;
			} 
			elseif ($minimo_factor == $minimo) {
				# code...
				return $minimo_factor;
			}
			else {
				# code...
				return $minimo;
			}
		}
		else{
			#vuelo nacional Nocturno 
			$minimo_factor = 1.97 * $this->peso * $ut;
			$minimo = 22.14 * $ut;
			if ($minimo_factor > $minimo) {
				# code...
				return $minimo_factor;
			} 
			elseif ($minimo_factor == $minimo) {
				# code...
				return $minimo_factor;
			}
			else {
				# code...
				return $minimo;
			}
		}
	}
}

}
