<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $fillable = [
        'nombre', 'matricula', 'tipo', 'token' ,'peso', 'ancho', 'capacidad_tripulacion',
    ];
}
