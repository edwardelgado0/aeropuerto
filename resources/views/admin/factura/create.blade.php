@extends('layouts.master')
@section('javascript')
<style type="text/css">
	/* The switch - the box around the slider */
.switch {
	position: absolute;
  	display: inline-block;
  	width: 60px;
  	height: 34px;

}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@endsection
@section('content')
<div class="main-grid">
		<div class="agile-grids">	
			<!-- input-forms -->
			<div class="grids">
				<form class="form-horizontal" action="/admin/factura" method="post">
					{{ csrf_field() }}
					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Aeronave y Cliente:</h4>
						</div>
						<div class="forms">
							<h3 class="title1"></h3>
							<div class="form-three widget-shadow">
								<div class="form-group">
									<label for="selector1" class="col-sm-2 control-label">Aeronave</label>
									<div class="col-sm-4">
										<select name="aircraft" id="Aircraft" class="form-control1">
											<option> </option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
										</select>
									</div>

									<label for="selector1" class="col-sm-1 control-label">Cliente</label>
									<div class="col-sm-5">
										<select name="cliente" id="Cliente" class="form-control1">
											<option> </option>
											<option>aeronave1</option>
											<option>aeronave2</option>
										</select>
									</div>
								</div>	
							</div>
						</div>
					</div>

					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Formulario Datos de Procedencia :</h4>
						</div>
						<div class="forms">
								<h3 class="title1"></h3>
								<div class="form-three widget-shadow">
									
									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">procedencia</label>
										<div class="col-sm-10">
											<select name="procedencia" id="selector1" class="form-control1">
												<option> </option>
												<option>aeronave1</option>
												<option>aeronave2</option>
										</select></div>
									</div>
									<div class="form-group">
										<label for="piloto" class="col-sm-2 control-label">piloto</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="piloto_procedencia" id="focusedinput" placeholder="Default Input">
										</div>
										<label for="focusedinput" class="col-sm-1 control-label">licencia</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="licencia_procedencia" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">pasajeros Desembarcados</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="pasajeros_desembarcados" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Transito</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="transito_procedencia" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Cargas Desembarcada</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="carga_desembarcada" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Fecha de Llegada</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="fecha_in" id="focusedinput" placeholder="Default Input">
										</div>
									</div>

									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">vuelo Numero</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
								</div>
						</div>
					</div>

					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Formulario Datos de Destino :</h4>
						</div>
						<div class="forms">
								<h3 class="title1"></h3>
								<div class="form-three widget-shadow">
									
									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">Destino</label>
										<div class="col-sm-10">
											<select name="selector1" id="selector1" class="form-control1">
												<option> </option>
												<option>aeronave1</option>
												<option>aeronave2</option>
										</select></div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">piloto</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<label for="focusedinput" class="col-sm-1 control-label">licencia</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">pasajeros emnbarcados</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Transito</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Cargas embarcada</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Fecha de Salida</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>

									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">vuelo Numero</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
								</div>
						</div>
					</div>
					<!-- -------------------------formulario aterrizaje ------------------- -->
					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Aterrizaje: </h4>
						</div>
						<div class="forms" id="aterrizaje_forms">
								<h3 class="title1"></h3>
								<div class="form-three widget-shadow">
									
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Aterrizaje:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control1" name="aterrizaje" id="Aterrizaje" placeholder="Default Input">
										</div>
										<div class="col-sm-1">
											<label class="switch">
												<input type="checkbox" class="checkbox">
												<div class="slider"></div>
											</label>
										</div>
									</div>

									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">Estacionamiento:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control1" name="estacionamiento" id="Estacionamiento" placeholder="Default Input">
										</div>
										<div class="col-sm-1">
											<label class="switch">
												<input type="checkbox">
												<div class="slider"></div>
											</label>
										</div>
									</div>

									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">Formulario:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control1" name="estacionamiento" id="Estacionamiento" placeholder="Default Input">
										</div>
										<div class="col-sm-1">
											<label class="switch">
												<input type="checkbox">
												<div class="slider"></div>
											</label>
										</div>
									</div>
									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">Multas:</label>
										<div class="col-sm-9">
											<input type="text" class="form-control1" name="multa" id="Multa" placeholder="Default Input">
										</div>
										<div class="col-sm-1">
											<label class="switch">
												<input type="checkbox" id="checkbox_multa">
												<div class="slider"></div>
											</label>
										</div>
									</div>

								</div>
						</div>
					</div>
					<!-- ------------------------ end formulario aterrizaje -->

					<!-- -------------------------formulario aterrizaje ------------------- -->
					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>estacionamiento: </h4>
						</div>
						<div class="forms">
								<h3 class="title1"></h3>
								<div class="form-three widget-shadow">
									
									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">Estacionamiento</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="estacionamiento" id="Estacionamiento" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									
								</div>
						</div>
					</div>
					<!-- ------------------------ end formulario aterrizaje -->

					<div class="panel panel-widget forms-panel">
						<div class="forms">
							<h3 class="title1"></h3>
							<div class="form-three widget-shadow">
								<div class="form-group">
									<div class="col-sm-offset-6"> 
												<button type="submit" class="btn btn-default w3ls-button">Enviar</button> 
											</div> 
									</div>
								</div>	
							</div>
						</div>
					</div>

				</form>
			</div>
			<!-- //input-forms -->
		</div>
	</div>

<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>

<script>


  jQuery(document).ready( function () {

  	multa = $('#checkbox_multa').is(':checked');
  	console.log(multa);
  	if($('#checkbox_multa').is(':checked')) {
    	console.log('Seleccionado');
	}
	if( $('.multaSwitch').attr('checked') ) {
    	console.log('Seleccionado');
	}

    // jQuery('#Inputphone').bind('enterKey', function (event) {});
    jQuery('#Aircraft').change(function(){
      // event.preventDefault();
        var aircraft = jQuery(this).val();
        console.log(aircraft);
        if (aircraft.length == 0 || aircraft.length == '') {
          window.alert('Debe rellenar el campo de teléfono');
          return;
        }

        var data_req = {
          'id': aircraft,
          'multa':multa,
          '_token': jQuery('input[name=_token]').val()
        };

        jQuery.post('/admin/factura/ajax-show', data_req)
          .done(function (response) {
            if (response.status == 'success') {
              var factura = response.data;
              var aterrizaje = response.aterrizaje;
              console.log(aterrizaje);
              if (!jQuery.isEmptyObject(factura)) {
                jQuery('#Aterrizaje').val(aterrizaje);
                jQuery('#Estacionamiento').val(factura.nombre);
                jQuery('#InputApellido').val(factura.peso);
                jQuery('#InputDireccion').val(factura.peso);
              }

            } else if (response.status == 'error') {
              window.alert(response.msg);
            }
        });
    });
  });
</script>
@endsection
	