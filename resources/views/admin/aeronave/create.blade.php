@extends('layouts.master')

@section('content')
<div class="main-grid">
		<div class="agile-grids">	
			<!-- input-forms -->
			<div class="grids">
				
				<div class="panel panel-widget forms-panel">
					<div class="progressbar-heading general-heading">
						<h4>Formulario Creación de Aeronave :</h4>
					</div>
					<div class="forms">
							<h3 class="title1"></h3>
							<div class="form-three widget-shadow">
								<form class="form-horizontal" action="/admin/aeronave" method="post">
									{{ csrf_field() }}
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">nombre</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="nombre" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Tipo de Aeronave</label>
										<div class="col-sm-8">
											<select id="estado" class="form-control select2" name="tipo" style="width: 100%;">
						                      <option selected="selected" value=" "></option>
						                          <option value="comercial">Aeronave Comercial</option>
						                          <option value="carga">Aeronave De Carga</option>
						                          <option value="militar">Aeronave Militar</option>
						                    </select>
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">matricula</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="matricula" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Peso</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="peso" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Fecha de llegada</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="fecha_llegada" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Fecha de Salida</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="fecha_salida" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
									
									<div class="col-sm-offset-2"> 
												<button type="submit" class="btn btn-default w3ls-button">Enviar</button> 
											</div> 
								</form>
							</div>
					</div>
				</div>
				
				
			</div>
			<!-- //input-forms -->
		</div>
	</div>

@endsection
	