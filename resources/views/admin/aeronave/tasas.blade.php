@extends('layouts.master')

@section('content')
<div class="main-grid">
		<div class="agile-grids">	
			<!-- input-forms -->
			<div class="grids">
				<form class="form-horizontal" action="#" method="post">

					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Aeronave y Cliente:</h4>
						</div>
						<div class="forms">
							<h3 class="title1"></h3>
							<div class="form-three widget-shadow">
								<div class="form-group">
									<label for="selector1" class="col-sm-2 control-label">Aeronave</label>
									<div class="col-sm-4">
										<select name="selector1" id="selector1" class="form-control1">
											<option> </option>
											<option>aeronave1</option>
											<option>aeronave2</option>
										</select>
									</div>

									<label for="selector1" class="col-sm-1 control-label">Cliente</label>
									<div class="col-sm-5">
										<select name="selector1" id="selector1" class="form-control1">
											<option> </option>
											<option>aeronave1</option>
											<option>aeronave2</option>
										</select>
									</div>
								</div>	
							</div>
						</div>
					</div>

					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Formulario Datos de Procedencia :</h4>
						</div>
						<div class="forms">
								<h3 class="title1"></h3>
								<div class="form-three widget-shadow">
									
									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">procedencia</label>
										<div class="col-sm-10">
											<select name="selector1" id="selector1" class="form-control1">
												<option> </option>
												<option>aeronave1</option>
												<option>aeronave2</option>
										</select></div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">piloto</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<label for="focusedinput" class="col-sm-1 control-label">licencia</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">pasajeros Desmnbarcados</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Transito</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Cargas Desembarcada</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Fecha de Llegada</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>

									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">vuelo Numero</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
								</div>
						</div>
					</div>

					<div class="panel panel-widget forms-panel">
						<div class="progressbar-heading general-heading">
							<h4>Formulario Datos de Destino :</h4>
						</div>
						<div class="forms">
								<h3 class="title1"></h3>
								<div class="form-three widget-shadow">
									
									<div class="form-group">
										<label for="selector1" class="col-sm-2 control-label">Destino</label>
										<div class="col-sm-10">
											<select name="selector1" id="selector1" class="form-control1">
												<option> </option>
												<option>aeronave1</option>
												<option>aeronave2</option>
										</select></div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">piloto</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<label for="focusedinput" class="col-sm-1 control-label">licencia</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">pasajeros emnbarcados</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Transito</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>
									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">Cargas embarcada</label>
										<div class="col-sm-4">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>

										<label for="focusedinput" class="col-sm-1 control-label">Fecha de Salida</label>
										<div class="col-sm-5">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
									</div>

									<div class="form-group">
										<label for="focusedinput" class="col-sm-2 control-label">vuelo Numero</label>
										<div class="col-sm-8">
											<input type="text" class="form-control1" name="Default Input" id="focusedinput" placeholder="Default Input">
										</div>
										<div class="col-sm-2">
											<p class="help-block">Your help text!</p>
										</div>
									</div>
								</div>
						</div>
					</div>

					<div class="panel panel-widget forms-panel">
						<div class="forms">
							<h3 class="title1"></h3>
							<div class="form-three widget-shadow">
								<div class="form-group">
									<div class="col-sm-offset-6"> 
												<button type="submit" class="btn btn-default w3ls-button">Enviar</button> 
											</div> 
									</div>
								</div>	
							</div>
						</div>
					</div>

				</form>
			</div>
			<!-- //input-forms -->
		</div>
	</div>

@endsection
	