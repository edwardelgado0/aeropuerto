@extends('layouts.master')

@section('javascript')
<link rel="stylesheet" type="text/css" href="{{ asset('css/table-style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/basictable.css') }}" />
<script type="text/javascript" src="{{ asset('js/jquery.basictable.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<!-- //tables -->
@endsection

@section('content')
<div class="main-grid">
			<div class="agile-grids">	
				<!-- tables -->
				
				<div class="table-heading">
					<h2>Basic Tables</h2>
				</div>
				<div class="agile-tables">
					<div class="w3l-table-info">
					  <h3>Basic Implementation</h3>
					    <table id="table">
						<thead>
						  <tr>
							<th>nombre</th>
							<th>typo</th>
							<th>Gender</th>
							<th>Height</th>
							<th>Province</th>
							<th>Sport</th>
							<th>Acción</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>Jill Smith</td>
							<td>25</td>
							<td>Female</td>
							<td>5'4</td>
							<td>British Columbia</td>
							<td>Volleyball</td>
							<td><a href="#">editar</a>|<a href="#">detalles</a></td>
						  </tr>
						  <tr>
							<td>John Stone</td>
							<td>30</td>
							<td>Male</td>
							<td>5'9</td>
							<td>Ontario</td>
							<td>Badminton</td>
							<td>editar|detalles</td>
						  </tr>
						  <tr>
							<td>Jane Strip</td>
							<td>29</td>
							<td>Female</td>
							<td>5'6</td>
							<td>Manitoba</td>
							<td>Hockey</td>
							<td>editar|detalles</td>
						  </tr>
						  <tr>
							<td>Gary Mountain</td>
							<td>21</td>
							<td>Male</td>
							<td>5'8</td>
							<td>Alberta</td>
							<td>Curling</td>
							<td>editar|detalles</td>
						  </tr>
						  <tr>
							<td>James Camera</td>
							<td>31</td>
							<td>Male</td>
							<td>6'1</td>
							<td>British Columbia</td>
							<td>Hiking</td>
							<td>editar|detalles</td>
						  </tr>
						</tbody>
					  </table>
					</div>
				</div>
				<!-- //tables -->
			</div>
		</div>

@endsection
	