<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftoriginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraftorigin', function(Blueprint $table){
            $table->Increments('id')->unsigned();
            //tabla procedencia
            $table->string('procedencia');
            $table->string('piloto_procedencia');
            $table->string('pasajeros_desembarcados');
            $table->string('transito');
            $table->string('licencia');
            $table->string('carga_desembarcada');

            $table->integer('aircraft_id')->unsigned();
            //$table->foreign('aircraft_id')->references('id')->on('aircrafts')->onDelete('cascade');
            $table->dateTime('fecha_in');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircrafts');
    }
}
