<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftdestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraftdestination', function(Blueprint $table){
            $table->Increments('id')->unsigned();
            //tabla procedencia
            $table->string('destino');
            $table->string('piloto_destino');
            $table->string('pasajeros_embarcados');
            $table->string('transito');
            $table->string('licencia');
            $table->string('carga_embarcada');

            $table->integer('aircraft_id')->unsigned();
            //$table->foreign('aircraft_id')->references('id')->on('aircrafts')->onDelete('cascade');
            $table->dateTime('fecha_out');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircraftdestination');
    }
}
