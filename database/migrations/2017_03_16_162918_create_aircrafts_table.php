<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAircraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircrafts', function(Blueprint $table){
            $table->Increments('id')->unsigned();
            $table->string('matricula');
            $table->string('nombre');
            $table->enum('tipo', ['comercial','carga','militares']);
            $table->string('peso');
            $table->string('largo');
            $table->string('ancho');
            $table->string('capacidad_tripulacion');
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircrafts');
    }
}
