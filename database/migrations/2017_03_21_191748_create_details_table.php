<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function(Blueprint $table){
            $table->Increments('id')->unsigned();
            $table->integer('factura_id');
            $table->integer('producto_id');
            $table->integer('cantidad');
            $table->integer('precio');
            /*
            $table->string('aterrizaje');
            $table->string('estacionamiento');
            $table->string('formulario');
            $table->string('multa');
            $table->string('nombre_servicio');
            $table->string('servicio');

            $table->string('permisovehiculo');
            $table->string('permisoasistencia');
            $table->string('autplataforma');
            $table->string('transbordo');
            $table->string('filmaciones');
            $table->string('carnet_permanente');
            $table->string('carnet_temporal');
            */
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
