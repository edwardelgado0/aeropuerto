<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Aircraft::class, function (Faker\Generator $faker) {
    return [
        'matricula' => $faker->randomDigitNotNull,
        'nombre' => $faker->name,
        'tipo' => 'comercial',
        'peso' => $faker->randomDigitNotNull,
        'largo' => $faker->randomDigitNotNull,
    ];
});

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'password' => bcrypt('secreto'),
        'email' => $faker->unique()->email,
    ];
});