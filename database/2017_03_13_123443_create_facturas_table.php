<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function(Blueprint $table){
            $table->Increments('id')->unsigned();
            $table->integer('aircraft_id');
            $table->integer('cliente_id');
            /*
            $table->string('aterrizaje');
            $table->string('estacionamiento');
            $table->string('formulario');
            $table->string('multa');
            $table->string('nombre_servicio');
            $table->string('servicio');

            $table->string('permisovehiculo');
            $table->string('permisoasistencia');
            $table->string('autplataforma');
            $table->string('transbordo');
            $table->string('filmaciones');
            $table->string('carnet_permanente');
            $table->string('carnet_temporal');
            */
            $table->integer('detail_id');
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
